// подключаем модули галпа
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');

//const watch = require('gulp-watch');

// Порядок подключения обработки sass файлов
const scssFiles= [
    './src/sass/reset.scss',
    './src/sass/main.scss',
    './src/sass/grid.scss'
]

// Порядок подключения css файлов
const cssFiles= [
    './src/css/reset.css',
    './src/css/main.css',
    './src/css/grid.css'
]

// Порядок подключения js файлов
const jsFiles= [
    './src/js/lib.js',
    './src/js/main.js']

// Таск на sass
 async function stylesass(){
    // Путь откуда брать scss файлы
    gulp.src(scssFiles)
        .pipe(sass().on('error', sass.logError))
        // Путь куда записывать сss стили
        .pipe(gulp.dest('./src/css'));
}


// Таск на стили
function styles() {
    // Шаблон для поиска файлов CSS
    // Все файлы по шаблону './src/css/*.css'
	return gulp.src(cssFiles)
    // Обьединение файлов в один
    .pipe(concat('style.css'))
    // Добавление автопрефиксов
    .pipe(autoprefixer({
        overrideBrowserslist: ["last 2 versions"],
        cascade: false
    }))
    // Минификация CSS
    .pipe(cleanCSS({
        level: 2
    }))
	// Выходная папка для стилей
    .pipe(gulp.dest('./build/css'))
    .pipe(browserSync.stream())
}

// Таск на стили
function scripts() {
    // Шаблон для поиска файлов JS
    // Все файлы по шаблону './src/js/*.js'
    return gulp.src(jsFiles)
    // Обьединение файлов в один
    .pipe(concat('script.js'))
    // Минификация JS
    .pipe(uglify({
        toplevel: true
    }))
    // Выходная папка для стилей
    .pipe(gulp.dest('./build/js'))
    .pipe(browserSync.stream())
}


// Удалить все в указанной папке
function clean(){
    return del(['build/*'])
}

// Просматривать файлы
function watch(){
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    // Следить за SCSS файлами
    gulp.watch('./src/sass/**/*.scss', stylesass)
    // Следить за CSS файлами
    gulp.watch('./src/css/**/*.css', styles)
    // Следить за JS файлами
    gulp.watch('./src/js/**/*.js', scripts)
    // При изменении HTML запускать синхронизацию
    gulp.watch("*/*.html").on('change', browserSync.reload)
}

// Таск вызывающий функции на SASS
gulp.task('sass', stylesass);

// Таск вызывающий функции на стили
gulp.task('styles', styles);

// Таск вызывающий функции на скрипты
gulp.task('scripts', scripts);

// Таск для очистки папки build
gulp.task('del', clean);

// Таск для отслеживания изменений
gulp.task('watch', watch);
// Таск для удаления файла build  и запуск styles и scripts
gulp.task('build', gulp.series(clean, stylesass, gulp.parallel(styles, scripts)));
// Таск запускает таск build и watch последовательно
gulp.task('dev', gulp.series('build','watch'));


